import React, { Component } from "react";
import { connect } from "react-redux";
import Feed from "./../components/feed";
import SendMessageForm from "./../components/sendMessageForm";
import { addMessage } from "./../actions/messages";
export class ChatContainer extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { userName, addMessage } = this.props;
    addMessage(`user ${userName}' is joined`, userName);
  }

  onMessageAdd = (message) => {
    const { userName, addMessage } = this.props;
    addMessage(message, userName);
  };

  render() {
    const { messages, userName } = this.props;
    return (
      <div class="chat">
        <div class="chat-header">
          <h2>Hello {userName}</h2>
        </div>
        <Feed messages={messages} userName={userName}></Feed>
        <SendMessageForm onMessageAdd={this.onMessageAdd}></SendMessageForm>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  messages: state.messages,
});

const mapDispatchToProps = {
  addMessage,
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatContainer);
