import React, { Component } from "react";

const ENTER_KEY_CODE = 13;

export default class SendMessageForm extends Component {
  constructor(props) {
    super(props);
    this.state = { message: "" };
  }

  handleFormChange = (e) => {
    this.setState({ message: e.target.value });
  };

  addMessage(message) {
    message && this.props.onMessageAdd(message);
  }

  handleMessageSend = () => {
    this.addMessage(this.state.message);
    this.setState({ message: "" });
  };

  handleKeyPress = (e) => {
    if (e.charCode === ENTER_KEY_CODE) {
      this.addMessage(this.state.message);
    }
  };

  render() {
    const { message } = this.state;
    return (
      <div className="message-form">
        <input
          type="text"
          className="message-input-field"
          value={message}
          onChange={this.handleFormChange}
          onKeyPress={this.handleKeyPress}
        />
        <input
          type="submit"
          className="message-submit-button"
          value="Send"
          onClick={this.handleMessageSend}
        />
      </div>
    );
  }
}
