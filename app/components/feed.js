import React, { Component } from "react";
import Message from "./message";

export default class Feed extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { messages, userName } = this.props;
    return (
      <div>
        <div className="feed">
          <div>
            {messages.map((message) => (
              <Message
                key={`${message.date}-${message.message}`}
                message={message}
                userName={userName}
              />
            ))}
          </div>
        </div>
      </div>
    );
  }
}
