import React, { Component } from "react";
import classnames from "classnames";
export default class Message extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { message, userName } = this.props;
    return (
      <div
        className={classnames("message", {
          left: message.userName === userName,
          right: message.userName !== userName,
        })}
      >
        <div className="message-header">
          <span>{message.userName}</span>
          <span>{message.date}</span>
        </div>

        <div>{message.message}</div>
      </div>
    );
  }
}
