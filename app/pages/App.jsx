import React from "react";
import { withRouter } from "react-router-dom";
import "notification-service-js";
import ChatContainer from "./../containers/ChatContainer";

class App extends React.Component {
  constructor() {
    super();

    this.state = { chats: ["user1", "user2"] };
  }

  handleAddRandomUserClick = () => {
    const { chats } = this.state;
    chats.push(`user ${Math.random()}`);
    this.setState({ chats });
  };

  render() {
    const { chats } = this.state;
    return (
      <div className="app">
        <div>
          <input
            type="submit"
            value="Add Random User"
            onClick={this.handleAddRandomUserClick}
          />
        </div>
        <div>
          {chats.map((chat) => (
            <ChatContainer userName={chat} key={chat} />
          ))}
        </div>
      </div>
    );
  }
}

export default withRouter(App);
