import { ADD_MESSAGE } from "../actions/messages";

/**
 * @param {Object} state
 * @param {Object} action
 * @returns {Object}
 */
export default function messages(state = [], action) {
  switch (action.type) {
    case ADD_MESSAGE:
      return [
        ...state,
        { ...action.payload, date: new Date().toLocaleString() },
      ];
    default:
      return state;
  }
}
