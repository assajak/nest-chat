export const ADD_MESSAGE = "ADD_MESSAGE";

export const addMessage = (message, userName) => ({
  type: ADD_MESSAGE,
  payload: { message, userName },
});
